package com.difinite.multifinanceemailservice.service;

import com.difinite.multifinanceemailservice.model.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import java.util.Properties;

@Component
public class EmailServiceImpl implements EmailService {

    @Autowired
    public JavaMailSender emailSender;

    @Override
    public void sendSimpleMessage(Email email) throws MessagingException {
        MimeMessage mailMessage = emailSender.createMimeMessage();

        mailMessage.setSubject(email.getSubject());
        mailMessage.setRecipients(Message.RecipientType.TO, email.getTo());
        mailMessage.setContent(email.getBody(), "text/html");
        emailSender.send(mailMessage);
    }

    @Primary
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("richardokusumasali@gmail.com");
        mailSender.setPassword("gcjfpdyqjivtwhud");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }
}
